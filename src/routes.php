<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// LOGIN
Route::get('admin/logout', array(
    'as'   => 'admin.logout',
    'uses' => 'AdminAuthController@getLogout'
 ));
Route::get('admin/login', array(
    'as'   => 'admin.login',
    'uses' => 'AdminAuthController@getLogin'
));
Route::post('admin/login', array(
    'as'   => 'admin.login.post',
    'uses' => 'AdminAuthController@postLogin'
));

// AUTHED
Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function() {

    Route::get('/', array(
        'as'   => 'admin',
        'uses' => 'AdminDefaultController@home'
    ));

    Route::group(array('before' => 'auth.admin.users'), function() {
        Route::post('users/action', array(
            'as'   => 'admin.users.action',
            'uses' => 'AdminUserController@action'
        ));
        Route::resource('users', 'AdminUserController');
    });

    Route::group(array('before' => 'auth.admin.groups'), function() {
        Route::post('groups/action', array(
            'as'   => 'admin.groups.action',
            'uses' => 'AdminGroupController@action'
        ));
        Route::resource('groups', 'AdminGroupController');
    });

    Route::group(array('before' => 'auth.admin.permissions'), function() {
        Route::get('permissions/csv', array(
            'as'   => 'admin.permissions.export',
            'uses' => 'AdminPermissionController@export'
        ));
        Route::post('permissions/action', array(
            'as'   => 'admin.permissions.action',
            'uses' => 'AdminPermissionController@action'
        ));
        Route::resource('permissions', 'AdminPermissionController');
    });

});
