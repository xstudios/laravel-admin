# X Studios Laravel Admin
####Author: Tim Santor <tsantor@xstudiosinc.com>

## Installation
Open your `composer.json` file and add the following to the `repositories` array:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/xstudios/laravel-admin.git"
        }
    ],

The add the folllowing to the `require` object:

    "xstudios/laravel-admin": "v0.2.3.1"

Or to stay on the bleeding edge (stability not guaranteed):

    "xstudios/laravel-admin": "dev-master"

### Install the dependencies

    php composer install

or

    php composer update

Once the package is installed you need to register the service provider with the application. Open up `app/config/app.php` and find the `providers` key.

    'providers' => array(
        # Other providers here ...
        'Xstudios\Laravel\Admin\AdminServiceProvider',
    )

## Configuration
### Config
To configure the package, you can use the following command to copy the configuration file to `app/config/packages/xstudios/laravel-admin`.

    php artisan config:publish xstudios/laravel-admin

> NOTE: The settings themselves are documented inside `config.php`.

### Update Database Seeder

    class DatabaseSeeder extends Seeder {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Eloquent::unguard();

            // Must be done first
            $this->call('SentrySeeder');
            $this->command->info('Sentry tables seeded!');

            // Must be done after Sentry
            $this->call('PermissionSeeder');
            $this->command->info('Permissions table seeded!');

            // Other seeders
            ...
        }

    }

### Run the Migrations

    php artisan migrate --package="cartalyst/sentry"
    php artisan migrate --package="xstudios/laravel-admin"
    php artisan migrate
    php artisan db:seed

### Publish Assets
The admin comes with some required assets in order to get you up and running
quickly.  These will be copied to `public/packages/xstudios/laravel-admin`.

    php artisan asset:publish xstudios/laravel-admin

### Create Custom Nav
The only other requirement to get started is to create the Admin's navbar view.
Create the file `app/views/admin/nav.blade.php` and you will need something
similar to the following:

    @if (Sentry::getUser()->hasAccess('permission.name'))
    <li class="">
        <a href="{{ URL::route('route.path') }}">My Page</a>
    </li>
    @endif

    // Other nav items here
    ...

### Publish Views (optional)
To override the default package views, you can use the following command to copy the view files to `app/config/packages/xstudios/laravel-admin`.

    php artisan view:publish xstudios/laravel-admin

## Extending the Admin
As far as the logic for extending the admin via controllers, for now, since I have
not created detailed documentation, just examine the package's controllers and views
or follow the example Laravel Skeleton (which also utilizes the laravel-admin package)
https://bitbucket.org/xstudios/laravel-skeleton.

## Contribute
In lieu of a formal styleguide, take care to maintain the existing coding style.

## License
MIT License (c) [X Studios](http://xstudiosinc.com)
