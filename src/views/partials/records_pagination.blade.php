<div class="text-center">
    {{ $records->getFrom() }} to {{ $records->getTo() }}  of <strong>{{ $records->getTotal() }}</strong><br/>
    {{ $records->addQuery('sortby', $sortby)->addQuery('order',$order)->links(); }}
</div>
