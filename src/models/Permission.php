<?php //namespace Xstudios\Laravel\Admin\Models;

use Illuminate\Support\Str;

class Permission extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    protected $fillable = array('label');

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('');

    // ------------------------------------------------------------------------

    /**
     * Returns an array suitable for using in a `<select>` list.
     *
     * @return array
     */
    public static function getSelectList() {
        $result = Permission::select('label', 'slug')
            ->orderBy('slug', 'asc')
            ->orderBy('label', 'asc')
            ->get();

        $select = array();
        foreach ($result as $_row) {
            $select[$_row->slug] = $_row->label;
        }

        // Remove SuperUser from permissions list
        array_pull($select, 'superuser');

        return $select;
    }

    // ------------------------------------------------------------------------

    /**
     * Returns an array of default permissions.
     *
     * @return array
     */
    public static function generateDefaultPermissions() {
        $default_content_types = array('user', 'group');
        $types = Config::get('Xstudios\Laravel\Admin::config.content_types');
        if ( ! empty($types))
        {
            $perms = array_merge($default_content_types, $types);
        }
        else
        {
            $perms = $default_content_types;
        }
        $actions = array('access', 'create', 'delete', 'update', 'view');

        $default_perms = array();
        foreach($perms as $perm)
        {
            $perm = str_replace('_', '.', strtolower($perm));
            foreach($actions as $action)
            {
                $label = str_replace('.', ' ', $perm);
                $label = sprintf('Can %s %s', $action, ucwords($label));
                $label = Str::plural($label);

                array_push($default_perms, array(
                    'label' => $label,
                    'slug'  => $perm.'.'.$action
                ));
            }
        }

        return $default_perms;
    }

    // ------------------------------------------------------------------------
}
