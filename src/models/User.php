<?php

//use Eloquent;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Support\Facades\DB;

class User extends \Cartalyst\Sentry\Users\Eloquent\User implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Determine if the user is in a group with the name specified.
     *
     * @return boolean
     */
    public function inGroupName($group) {
        $group = Sentry::findGroupByName($group);
        return $this->inGroup($group);
    }

    /**
     * Get the is_staff status of the user.
     *
     * @return boolean
     */
    /*public function isStaff() {
        return $this->is_staff;
    }*/

    // ------------------------------------------------------------------------
    // TODO: NEED TO BE IMPLEMENTED
    // ------------------------------------------------------------------------
    public function getRememberToken() {
        return false;
    }

    public function setRememberToken($value) {

    }

    public function getRememberTokenName() {
        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Returns an array suitable for using in a `<select>` list.
     *
     * @return array
     */
    public static function getSelectList() {
        $result = User::select('first_name', 'last_name', 'id')
            ->orderBy('last_name', 'asc')
            ->get();

        $select = array();
        foreach ($result as $_row) {
            $select[$_row->id] = $_row->first_name . ' ' . $_row->last_name;
        }

        return $select;
    }

    // ------------------------------------------------------------------------

}
