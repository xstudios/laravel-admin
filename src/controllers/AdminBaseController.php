<?php

class AdminBaseController extends Controller {

    /**
     * The logged in user (our User instance, not Sentry's)
     *
     * @var User
     */
    protected $_user;

    /**
     * The object type we are working on (eg - 'user')
     *
     * @var string
     */
    protected $_object;

    /**
     * Base route name
     *
     * @var string
     */
    protected $_route_name;

    // Routes
    protected $_route_index;
    protected $_route_create;
    protected $_route_edit;
    protected $_route_delete;
    protected $_route_show;

    // Views
    protected $_view_index;
    protected $_view_create;
    protected $_view_edit;
    protected $_view_delete;
    protected $_view_show;

    // Config
    protected $_route_admin;
    protected $_view_prefix;
    protected $_per_page;

    // ------------------------------------------------------------------------

    function __construct() {
        //parent::__construct();
        //$this->_user = Sentry::getUser();
        //View::share('user', $this->_user);

        // Set configurable options
        $this->_route_admin = $this->_route_admin ? $this->_route_admin : Config::get('Xstudios\Laravel\Admin::config.route_admin');
        $this->_view_prefix = $this->_view_prefix ? $this->_view_prefix : Config::get('Xstudios\Laravel\Admin::config.view_prefix');
        $this->_per_page    = $this->_per_page ? $this->_per_page : Config::get('Xstudios\Laravel\Admin::config.per_page');

        // Set typical CRUD routes
        $this->_route_index  = sprintf('admin.%s.index', $this->_route_name);
        $this->_route_create = sprintf('admin.%s.create', $this->_route_name);
        $this->_route_edit   = sprintf('admin.%s.edit', $this->_route_name);
        $this->_route_delete = sprintf('admin.%s.destroy', $this->_route_name);
        $this->_route_show   = sprintf('admin.%s.show', $this->_route_name);

        // Set view paths
        $this->_view_index  = sprintf('%s%s.index', $this->_view_prefix, $this->_route_name);
        $this->_view_create = sprintf('%s%s.create', $this->_view_prefix, $this->_route_name);
        $this->_view_edit   = sprintf('%s%s.edit', $this->_view_prefix, $this->_route_name);
        $this->_view_delete = sprintf('%s%s.destroy', $this->_view_prefix, $this->_route_name);
        $this->_view_show   = sprintf('%s%s.show', $this->_view_prefix, $this->_route_name);
    }

    // ------------------------------------------------------------------------

    /**
     * Redirect the user after save.
     *
     * @param  string  $label  descriptive label of what was saved
     * @param  integer $id     model ID
     * @return Redirect
     */
    protected function _afterSave($label, $id)
    {
        // Save message
        $save_msg = sprintf('The %s <strong>"%s"</strong> was saved successfully.', $this->_object, $label);

        // Save and continue
        if (Input::get('save_and_continue', false))
        {
            Session::flash('message', array(
                'class'   => 'success',
                'message' => $save_msg.' You may edit it again below.'
            ));
            return Redirect::route($this->_route_edit, $id);
        }

        // Save and new
        if (Input::get('save_and_new', false))
        {
            Session::flash('message', array(
                'class'   => 'success',
                'message' => sprintf($save_msg.' You may add another %s below.', $this->_object)
            ));
            return Redirect::route($this->_route_create);
        }

        // Save (default)
        Session::flash('message', array(
            'class'   => 'success',
            'message' => $save_msg
        ));
        return Redirect::route($this->_route_index);
    }

    // ------------------------------------------------------------------------

    /**
     * Upload a file
     *
     * @param  File    $file
     * @param  string  $dif
     * @return string or false
     */
    protected function _upload($file, $dir)
    {
        $destination = Config::get('Xstudios\Laravel\Admin::config.upload_path') . $dir;
        $filename    = $file->getClientOriginalName();
        $path        = '/' . $dir . '/' . $filename;
        $uploaded    = $file->move($destination, $filename);

        if ($uploaded)
        {
            return $path;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resources from storage.
     *
     * @param  array  $records
     * @return void
     */
    protected function _destroyMultiple($records) {
        foreach ($records as $id) {
            $this->destroy($id);
        }

        Session::flash('message', array(
            'class'   => 'success',
            'message' => sprintf('%s deleted successfully.', ucfirst(str_plural($this->_object)))
        ));
    }

    // ------------------------------------------------------------------------

    /**
     * Do a custom action to multiple records.
     *
     * @return Response
     */
    public function action() {
        $records = Input::get('records');
        $action  = Input::get('action');

        if (!is_array($records))
        {
            $msg = 'You must choose at least one %s to perform an action on.';
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => sprintf($msg, ucfirst($this->_object))
            ));
            return Redirect::route($this->_route_index);
        }

        try
        {
            switch($action)
            {
                case 'delete':
                    $this->_destroyMultiple($records);
                    break;

                default:
                    // Call user defined function in the form of `_myFunction`
                    // NOTE: Don't forget to call Redirect::route('route.name')
                    $method = '_'.$action;
                    if (method_exists($this, $method))
                    {
                        return $this->$method($records);
                    }
                    else
                    {
                        Session::flash('message', array(
                            'class'   => 'danger',
                            'message' => 'No method has been defined for this action.'
                        ));
                        return Redirect::route($this->_route_index);
                    }
            }
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage().' in '.$e->getFile(). ':'.$e->getLine());
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'There was an error attempting this action. The error has been logged.'
            ));
            return Redirect::route($this->_route_index);
        }

        return Redirect::route($this->_route_index);
    }

    // ------------------------------------------------------------------------

}
