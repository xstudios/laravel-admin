@extends('Xstudios\Laravel\Admin::layout/base')

@section('breadcrumbs')
    @parent
    <li><a href="{{ URL::route('admin.users.index') }}">Users</a></li>
@stop
