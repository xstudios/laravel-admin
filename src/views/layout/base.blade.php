<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ Config::get('Xstudios\Laravel\Admin::config.admin_title') }}</title>

        <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon">

        <link rel="stylesheet" media="all" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
        <link rel="stylesheet" media="all" href="{{ URL::asset('packages/xstudios/laravel-admin/css/admin.css') }}" />
        <link rel="stylesheet" media="all" href="{{ URL::asset('packages/xstudios/laravel-admin/css/jasny-bootstrap.min.css') }}" />
        @yield('css')

        @yield('js')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            @section('header')
                @include('Xstudios\Laravel\Admin::layout/header')
            @show

            <div id="body-content">
                @section('content')
                    <div class="page-header">
                        <h1>
                            @yield('page-header')
                        </h1>
                        <p class="lead">
                            @yield('page-lead')
                        </p>
                    </div>

                    @include('Xstudios\Laravel\Admin::partials/alert')
                @show
            </div> <!-- /#body-content -->

            @section('footer')
                @include('Xstudios\Laravel\Admin::layout/footer')
            @show

        </div> <!-- /container -->

        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Grab Google CDN jQuery...fall back to local -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>!window.jQuery && document.write("<script src=\"{{ URL::asset('js/jquery.js') }}\"><\/script>")</script>
        <!-- Bootstrap core JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="{{ URL::asset('packages/xstudios/laravel-admin/js/jasny-bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('packages/xstudios/laravel-admin/js/admin/admin.min.js') }}"></script>
        <script src="{{ URL::asset('packages/xstudios/laravel-admin/js/bootstrap-datepicker.min.js') }}"></script>
        @yield('body_js')

    </body>
</html>
