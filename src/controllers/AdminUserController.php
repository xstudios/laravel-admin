<?php

class AdminUserController extends AdminBaseController {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        //$this->beforeFilter('auth.admin.users');

        $this->_object      = 'user';
        $this->_route_name  = 'users';
        $this->_view_prefix = 'Xstudios\Laravel\Admin::';

        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('user.access') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        // Get sortable data
        $sortby = Input::get('sortby', 'last_name');
        $order  = Input::get('order', 'asc');
        $page   = Input::get('page');

        $records = User::orderBy($sortby, $order)
            ->paginate($this->_per_page);

        // Custom actions  in the form of ('key' => 'value')
        // to be handled by the 'action' method defined below.
        $actions = array();

        return View::make($this->_view_index,
            compact('records', 'sortby', 'order', 'page', 'actions')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('user.create') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        // Get the data
        $groups = Group::getSelectList();
        $permissions = Permission::getSelectList();

        return View::make($this->_view_create,
            compact('groups', 'permissions')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Validate
        $rules = array(
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|email',
            'password'   => 'required|confirmed'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }

        try
        {
            // Create the user
            $user = Sentry::createUser(array(
                'first_name'  => Input::get('first_name'),
                'last_name'   => Input::get('last_name'),
                'email'       => Input::get('email'),
                'password'    => Input::get('password'),
                'activated'   => Input::get('activated', false),
                'is_staff'    => Input::get('is_staff', false),
                'permissions' => $this->_setPermissions()
            ));

            // Assign any groups to the user
            $this->_setGroups($user);
        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => sprintf('User with login <strong>"%s"</strong> already exists.', Input::get('email'))
            ));
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Group was not found.'
            ));
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }

        // Redirect
        return $this->_afterSave($user->first_name.' '.$user->last_name, $user->id);
    }

    // ------------------------------------------------------------------------

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('user.view') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        echo 'Not implemented';
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('user.update') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        // Get the data
        $user        = Sentry::findUserById($id);
        $user_perms  = $user->getMergedPermissions();
        $user_groups = $user->getGroups()->lists('id');

        $permissions = Permission::getSelectList();
        $groups      = Group::getSelectList();

        // Set selected groups
        $selected_groups = array();
        foreach ($user_groups as $group)
        {
            if (array_key_exists($group, $groups))
            {
                array_push($selected_groups, $group);
            }
        }

        // Set selected perms
        $selected_perms = array();
        foreach ($user_perms as $k => $v)
        {
            if (array_key_exists($k, $permissions))
            {
                if ($v == 0 || $v == 1) {
                    array_push($selected_perms, $k);
                }
            }
        }

        // Show view
        return View::make($this->_view_edit,
            compact('user', 'groups', 'permissions', 'selected_groups', 'selected_perms')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Validate
        $rules = array(
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|email'
        );

        // If user is attempting to change password, add addiitonal rules
        if (Input::get('password'))
        {
            $rules['password'] = 'required|confirmed';
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }

        try
        {
            // Find the user using the user id
            $user = Sentry::findUserById($id);

            // Assign any groups to the user
            $this->_setGroups($user);

            // Update the user details
            $user->first_name = Input::get('first_name');
            $user->last_name  = Input::get('last_name');

            if ($user->email != Input::get('email'))
            {
                $user->email = Input::get('email');
            }

            // Set checkbox values
            $user->activated = Input::get('activated', false);
            $user->is_staff  = Input::get('is_staff', false);

            // Assign permissions
            $user->permissions = $this->_setPermissions();

            // If user is attempting to change password
            if (isset($rules['password'])) {
                $user->password = Input::get('password');
            }

            /*if (Input::get('current_password'))
            {
                if ($user->checkPassword(Input::get('current_password')))
                {
                    $user->password = Input::get('password');
                }
                else
                {
                    Session::flash('message', array(
                        'class'   => 'danger',
                        'message' => 'Your current password does not match.'
                    ));
                    return Redirect::route($this->_route_edit, $id)
                        ->withErrors($validator)
                        ->withInput(Input::except('password'));
                }
            }*/

            $user->save();

            // Redirect
            return $this->_afterSave($user->first_name.' '.$user->last_name, $user->id);

        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => sprintf('User with login <strong>"%s"</strong> already exists.', Input::get('email'))
            ));
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'User was not found.'
            ));
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Group was not found.'
            ));
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('user.delete') )
        {
            // Return JSON
            return Response::json(array('result'=>false));
        }

        try
        {
            // Delete record
            $record = Sentry::findUserById($id);
            $record->delete();

            // Return JSON
            return Response::json(array('result'=>true));
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            // Return JSON
            return Response::json(array('result'=>false));
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Return a group list.
     *
     * @return array
     */
    private function _setGroups($user) {
        $groups = Group::all()->lists('id');
        if (Input::get('group-select', false))
        {
            foreach($groups as $group)
            {
                $adminGroup = Sentry::findGroupById($group);
                if (in_array($group, Input::get('group-select')))
                {
                    $user->addGroup($adminGroup);
                }
                else
                {
                    $user->removeGroup($adminGroup);
                }
            }
        }
        else
        {
            foreach($groups as $group)
            {
                $adminGroup = Sentry::findGroupById($group);
                $user->removeGroup($adminGroup);
            }
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Return a permission list.
     *
     * @return array
     */
    private function _setPermissions() {
        $perms_list = array();

        // Security check, only users with permission can assign permissions
        if ( ! Sentry::getUser()->hasAccess('user.permission.assign') )
        {
            return $perms_list;
        }

        $permissions = Permission::lists('slug');

        // Remove superuser from permissions for security
        // We only add it if it's checked explicitly and the user is
        // a SuperUser (which means they can designate other SuperUsers)
        array_pull($permissions, 'superuser');

        if (Input::get('permission-select', false))
        {
            foreach($permissions as $perm)
            {
                if (in_array($perm, Input::get('permission-select')))
                {
                    $perms_list[$perm] = 1; // allow
                }
                else
                {
                    $perms_list[$perm] = -1; // deny
                }

            }
        }
        else
        {
            foreach($permissions as $perm)
            {
                $perms_list[$perm] = 0; // inherit from group
            }
        }

        // Add/Remove SuperUser permission
        if (Sentry::getUser()->isSuperUser())
        {
            if (Input::get('is_superuser', false))
            {
                $perms_list['superuser'] = 1;
            }
            else
            {
                $perms_list['superuser'] = -1;
            }
        }

        return $perms_list;
    }

    // ------------------------------------------------------------------------

}
