<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //
});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

// Admin filters
Route::filter('auth.admin.users', function()
{
    if ( ! Sentry::getUser()->hasAccess('user.access') )
    {
        Session::flash('message', array(
            'class'   => 'danger',
            'message' => 'Permission denied!'
        ));
        return Redirect::route('admin');
    }
});

Route::filter('auth.admin.groups', function()
{
    if ( ! Sentry::getUser()->hasAccess('group.access') )
    {
        Session::flash('message', array(
            'class'   => 'danger',
            'message' => 'Permission denied!'
        ));
        return Redirect::route('admin');
    }
});

Route::filter('auth.admin.permissions', function()
{
    if ( ! Sentry::getUser()->isSuperUser() )
    {
        Session::flash('message', array(
            'class'   => 'danger',
            'message' => 'Permission denied!'
        ));
        return Redirect::route('admin');
    }
});
