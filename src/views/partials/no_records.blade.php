@if ( !$records->count())
<div class="alert alert-info" role="alert">
    <strong>There were no records found.</strong>
</div>
@endif
