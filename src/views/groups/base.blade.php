@extends('Xstudios\Laravel\Admin::layout/base')

@section('breadcrumbs')
    @parent
    <li><a href="{{ URL::route('admin.groups.index') }}">Groups</a></li>
@stop
