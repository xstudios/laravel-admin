@extends('Xstudios\Laravel\Admin::permissions/base')

@section('page-header')
    Permissions
@stop

@section('page-lead')
    This area allows you to add/edit and delete permissions.
@stop

@section('content')

    @parent

    <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav">

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Sentry::getUser()->hasAccess('permission.export'))
                    <li><a href="{{ URL::route('admin.permissions.export') }}"><span class="glyphicon glyphicon-save"></span> Export</a></li>
                    @endif

                    @if (Sentry::getUser()->hasAccess('permission.create'))
                    <li><a href="{{ URL::route('admin.permissions.create') }}"><span class="glyphicon glyphicon-plus"></span> Create New</a></li>
                    @endif

                </ul>

            </div> <!--/.nav-collapse -->
        </div> <!--/.container-fluid -->
    </div> <!-- /.navbar -->

    {{ Form::open(array('route' => 'admin.permissions.action', 'id' => 'form-records')) }}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{ Form::checkbox('', '', false, array('id'=>'select-all-records')) }}</th>
                    <th>{{ ViewHelper::sortable('Label', 'label', $sortby, $order, $page, 'admin.permissions.index') }}</th>
                    <th>{{ ViewHelper::sortable('Slug', 'slug', $sortby, $order, $page, 'admin.permissions.index') }}</th>
                    <th>Updated</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach($records as $permission)
                <tr>
                    <td>{{ Form::checkbox('records[]', $permission->id) }}</td>
                    <td>{{ $permission->label }}</td>
                    <td>{{ $permission->slug }}</td>
                    <td>{{ $permission->updated_at }}</td>
                    <td>
                        <!--<a class="btn btn-xs btn-default" href="{{ URL::route('admin.permissions.show', $permission->id) }}"><span class="glyphicon glyphicon-eye-open"></span> View</a>-->

                        @if (Sentry::getUser()->hasAccess('permission.update'))
                        <a class="btn btn-xs btn-default" href="{{ URL::route('admin.permissions.edit', $permission->id) }}"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                        @endif

                        @if (Sentry::getUser()->hasAccess('permission.delete'))
                        <button class="btn btn-xs btn-danger" onclick="admin.deleteRow('{{ URL::route('admin.permissions.destroy', $permission->id)}}', $(this)); $('#record-name').html('{{ $permission->slug }}'); return false;" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                        @endif

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @include('Xstudios\Laravel\Admin::partials/index_actions')
    {{ Form::close() }}

    @include('Xstudios\Laravel\Admin::partials/no_records')

    @include('Xstudios\Laravel\Admin::partials/records_pagination')

    @include('Xstudios\Laravel\Admin::partials/delete_modal')

@stop


@section('footer')

@stop
