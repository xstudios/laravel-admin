<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Page/Admin Title
    |--------------------------------------------------------------------------
    |
    | The page / admin title.
    |
    */

    'admin_title' => 'Laravel Skeleton Admin',

    /*
    |--------------------------------------------------------------------------
    | Admin Route
    |--------------------------------------------------------------------------
    |
    | The named route to your admin.
    |
    */

    'route_admin' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Admin Default Route
    |--------------------------------------------------------------------------
    |
    | When a user logs in, this is where we send them.
    |
    */

    'default_route' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Admin View Prefix
    |--------------------------------------------------------------------------
    |
    | The prefix to add to your view names.
    | (eg - 'admin.users.index')
    |
    */
    'view_prefix' => 'admin.',

    /*
    |--------------------------------------------------------------------------
    | Pagination Per Page Limit
    |--------------------------------------------------------------------------
    |
    | The number of entires to show per page using pagination.
    |
    */
    'per_page' => 20,

    /*
    |--------------------------------------------------------------------------
    | Upload Path
    |--------------------------------------------------------------------------
    |
    | The default path for file uploads.  Ensure this directory exists
    | and is writable.
    |
    */

    'upload_path' => public_path() . '/uploads/',

    /*
    |--------------------------------------------------------------------------
    | Custom Content Types
    |--------------------------------------------------------------------------
    |
    | This array of additional content types to add to the default permissions.
    | These valus should be all lowercase and singular.  All content types will
    | get 'access', 'create', 'delete', 'update' and 'view' permissions.
    |
    */

    'content_types' => array(

    ),

);
