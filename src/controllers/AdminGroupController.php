<?php

class AdminGroupController extends AdminBaseController {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        //$this->beforeFilter('auth.admin.groups');

        $this->_object      = 'group';
        $this->_route_name  = 'groups';
        $this->_view_prefix = 'Xstudios\Laravel\Admin::';

        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Get sortable data
        $sortby = Input::get('sortby');
        $order  = Input::get('order');
        $page   = Input::get('page');

        if ($sortby && $order)
        {
            $records = Group::orderBy($sortby, $order)
                ->paginate($this->_per_page);
        }
        else
        {
            $records = Group::orderBy('name', 'asc')
                ->paginate($this->_per_page);
        }

        // Custom actions  in the form of ('key' => 'value')
        // to be handled by the 'action' method defined below.
        $actions = array();

        return View::make($this->_view_index,
            compact('records', 'sortby', 'order', 'page', 'actions')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('group.create') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        $permissions = Permission::getSelectList();
        return View::make($this->_view_create, compact('permissions'));
    }

    // ------------------------------------------------------------------------

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Validate
        $rules = array(
            'name' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput();
        }

        try
        {
            // Create the group
            $group = Sentry::createGroup(array(
                'name'        => Input::get('name'),
                'permissions' => $this->_setPermissions()
            ));
        }
        catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'A group with that name already exists.'
            ));
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput();
        }

        // Redirect
        return $this->_afterSave($group->name, $group->id);
    }

    // ------------------------------------------------------------------------

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('group.view') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        echo 'Not implemented';
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('group.update') )
        {
            Session::flash('message', array(
                'class'   => 'danger',
                'message' => 'Permission denied!'
            ));
            return Redirect::route($this->_route_admin);
        }

        // Get the data
        $group       = Sentry::findGroupById($id);
        $group_perms = $group->getPermissions();
        $permissions = Permission::getSelectList();

        // Set selected perms
        $selected_perms = array();
        foreach ($group_perms as $k => $v)
        {
            if (array_key_exists($k, $permissions))
            {
                array_push($selected_perms, $k);
            }
        }

        // Show view
        return View::make($this->_view_edit,
            compact('group', 'permissions', 'selected_perms')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Validate
        $rules = array(
            'name' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput();
        }

        // Find the group using the group id
        $group = Sentry::findGroupById($id);

        // Update the group details
        $group->name = Input::get('name');
        $group->permissions = $this->_setPermissions();
        $group->save();

        // Redirect
        return $this->_afterSave($group->name, $group->id);
    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Access control
        if ( ! Sentry::getUser()->hasAccess('group.delete') )
        {
            // Return JSON
            return Response::json(array('result'=>false));
        }

        // Delete record
        $record = Sentry::findGroupById($id);
        $record->delete();

        // Return JSON
        return Response::json(array('result'=>true));
    }

    // ------------------------------------------------------------------------

    /**
     * Return a permission list.
     *
     * @return array
     */
    private function _setPermissions() {
        $permissions = Permission::lists('slug');
        $perms_list = array();
        if (Input::get('permission-select', false))
        {
            foreach($permissions as $perm)
            {
                if (in_array($perm, Input::get('permission-select')))
                {
                    $perms_list[$perm] = 1; // allow
                }
                else
                {
                    $perms_list[$perm] = 0; // deny
                }

            }
        }
        else
        {
            foreach($permissions as $perm)
            {
                $perms_list[$perm] = 0; // deny
            }
        }

        return $perms_list;
    }

    // ------------------------------------------------------------------------

}
