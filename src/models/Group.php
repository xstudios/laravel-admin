<?php

class Group extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('');

    // ------------------------------------------------------------------------

    /**
     * Returns an array suitable for using in a `<select>` list.
     *
     * @return array
     */
    public static function getSelectList() {
        $result = Group::select('name', 'id')
            ->orderBy('name', 'asc')
            ->get();

        $select = array();
        foreach ($result as $_row) {
            $select[$_row->id] = $_row->name;
        }

        return $select;
    }

    // ------------------------------------------------------------------------

}
