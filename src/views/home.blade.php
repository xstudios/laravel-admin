@extends('Xstudios\Laravel\Admin::layout/base')

@section('content')

    @parent

    <div class="jumbotron">
        <h1>Welcome, Admin!</h1>
        <p>This area allows you to manage all aspects of your site.</p>
    </div>

@stop
