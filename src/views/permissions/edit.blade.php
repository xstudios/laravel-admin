@extends('Xstudios\Laravel\Admin::permissions/base')

@section('breadcrumbs')
    @parent
    <li class="active">Edit</li>
@stop

@section('page-header')
    Edit Permission
@stop

@section('content')
    @parent

    <!-- if there are validation errors, they will show here -->
    @if ($errors->all())
    <div class="alert alert-danger">
    {{ HTML::ul($errors->all()) }}
    </div>
    @endif

    {{ Form::model($permission, array('route' => array('admin.permissions.update', $permission->id), 'method' => 'PUT', 'class'=>'form-horizontal')) }}

        <div class="well">
            {{ Form::bootstrapTextControl('Label', 'label') }}
            {{ Form::bootstrapTextControl('Slug', 'slug') }}
        </div>

        <div class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container-fluid admin-form-controls">
                {{ Form::bootstrapSaveButtons('admin.permissions.index') }}
                <div class="clearfix"></div>
            </div>
        </div>

    {{ Form::close() }}

@stop

@section('footer')

@stop
