@extends('Xstudios\Laravel\Admin::users/base')

@section('page-header')
    Users
@stop

@section('page-lead')
    This area allows you to add/edit and delete users.
@stop

@section('content')

    @parent

    <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav">

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Sentry::getUser()->hasAccess('user.create'))
                    <li><a href="{{ URL::route('admin.users.create') }}"><span class="glyphicon glyphicon-plus"></span> Create New</a></li>
                    @endif
                </ul>

            </div> <!--/.nav-collapse -->
        </div> <!--/.container-fluid -->
    </div> <!-- /.navbar -->

    {{ Form::open(array('route' => 'admin.users.action', 'id' => 'form-records')) }}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{ Form::checkbox('', '', false, array('id'=>'select-all-records')) }}</th>
                    <th>{{ ViewHelper::sortable('First Name', 'first_name', $sortby, $order, $page, 'admin.users.index') }}</th>
                    <th>{{ ViewHelper::sortable('Last Name', 'last_name', $sortby, $order, $page, 'admin.users.index') }}</th>
                    <th>{{ ViewHelper::sortable('Email', 'email', $sortby, $order, $page, 'admin.users.index') }}</th>
                    <th>{{ ViewHelper::sortable('Activated', 'activated', $sortby, $order, $page, 'admin.users.index') }}</th>
                    <th>Updated</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach($records as $user)
                <tr>
                    <td>{{ Form::checkbox('records[]', $user->id) }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>@if ($user->activated)<span class="glyphicon glyphicon-ok"></span>@else<span class="glyphicon glyphicon-remove"></span>@endif</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        <!--<a class="btn btn-xs btn-default" href="{{ URL::route('admin.users.show', $user->id) }}"><span class="glyphicon glyphicon-eye-open"></span> View</a>-->

                        @if (Sentry::getUser()->hasAccess('user.update'))
                        <a class="btn btn-xs btn-default" href="{{ URL::route('admin.users.edit', $user->id) }}"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                        @endif

                        @if (Sentry::getUser()->hasAccess('user.delete'))
                        <button class="btn btn-xs btn-danger" onclick="admin.deleteRow('{{ URL::route('admin.users.destroy', $user->id)}}', $(this)); $('#record-name').html('{{ $user->email }}'); return false;" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                        @endif

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @include('Xstudios\Laravel\Admin::partials/index_actions')
    {{ Form::close() }}

    @include('Xstudios\Laravel\Admin::partials/no_records')

    @include('Xstudios\Laravel\Admin::partials/records_pagination')

    @include('Xstudios\Laravel\Admin::partials/delete_modal')

@stop

@section('footer')

@stop
