@extends('Xstudios\Laravel\Admin::groups/base')

@section('page-header')
    Groups
@stop

@section('page-lead')
    This area allows you to add/edit and delete groups.
@stop

@section('content')

    @parent

    <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav">

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Sentry::getUser()->hasAccess('group.create'))
                    <li><a href="{{ URL::route('admin.groups.create') }}"><span class="glyphicon glyphicon-plus"></span> Create New</a></li>
                    @endif
                </ul>

            </div> <!--/.nav-collapse -->
        </div> <!--/.container-fluid -->
    </div> <!-- /.navbar -->

    {{ Form::open(array('route' => 'admin.groups.action', 'id' => 'form-records')) }}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{ Form::checkbox('', '', false, array('id'=>'select-all-records')) }}</th>
                    <th>{{ ViewHelper::sortable(' Name', 'name', $sortby, $order, $page, 'admin.groups.index') }}</th>
                    <th>Updated</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach($records as $group)
                <tr>
                    <td>{{ Form::checkbox('records[]', $group->id) }}</td>
                    <td>{{ $group->name }}</td>
                    <td>{{ $group->updated_at }}</td>
                    <td>
                        <!--<a class="btn btn-xs btn-default" href="{{ URL::route('admin.groups.show', $group->id) }}"><span class="glyphicon glyphicon-eye-open"></span> View</a>-->

                        @if (Sentry::getUser()->hasAccess('group.update'))
                        <a class="btn btn-xs btn-default" href="{{ URL::route('admin.groups.edit', $group->id) }}"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                        @endif

                        @if (Sentry::getUser()->hasAccess('group.delete'))
                        <button class="btn btn-xs btn-danger" onclick="admin.deleteRow('{{ URL::route('admin.groups.destroy', $group->id)}}', $(this)); $('#record-name').html('{{ addslashes($group->name) }}'); return false;" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                        @endif

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @include('Xstudios\Laravel\Admin::partials/index_actions')
    {{ Form::close() }}

    @include('Xstudios\Laravel\Admin::partials/no_records')

    @include('Xstudios\Laravel\Admin::partials/records_pagination')

    @include('Xstudios\Laravel\Admin::partials/delete_modal')

@stop

@section('footer')

@stop
