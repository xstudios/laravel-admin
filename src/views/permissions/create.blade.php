@extends('Xstudios\Laravel\Admin::permissions/base')

@section('breadcrumbs')
    @parent
    <li class="active">Create</li>
@stop

@section('page-header')
    Create Permission
@stop

@section('content')
    @parent

    <!-- if there are validation errors, they will show here -->
    @if ($errors->all())
    <div class="alert alert-danger">
    {{ HTML::ul($errors->all()) }}
    </div>
    @endif

    {{ Form::open(array('route' => 'admin.permissions.index', 'class' => 'form-horizontal')) }}

        <div class="well">
            {{ Form::bootstrapTextControl('Label', 'label') }}
            {{ Form::bootstrapTextControl('Slug', 'slug') }}
        </div>

        <div class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container-fluid admin-form-controls">
                {{ Form::bootstrapSaveButtons('admin.permissions.index') }}
                <div class="clearfix"></div>
            </div>
        </div>

    {{ Form::close() }}

@stop

@section('footer')

@stop
