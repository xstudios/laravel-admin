<?php

class AdminPermissionController extends AdminBaseController {

    private $_rules = array(
        'label' => 'required',
        'slug'  => 'required'
    );

    // ------------------------------------------------------------------------

    public function __construct()
    {
        //$this->beforeFilter('auth.admin.permissions');

        $this->_object      = 'permission';
        $this->_route_name  = 'permissions';
        $this->_view_prefix = 'Xstudios\Laravel\Admin::';

        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Get sortable data
        $sortby = Input::get('sortby', 'slug');
        $order  = Input::get('order', 'asc');
        $page   = Input::get('page');

        $records = Permission::where('slug', '!=', 'superuser')
                ->orderBy($sortby, $order)
                //->orderBy('label', 'asc')
                ->paginate($this->_per_page);
        /*
        if ($sortby && $order)
        {
            $records = Permission::where('slug', '!=', 'superuser')
                ->orderBy($sortby, $order)
                ->paginate($this->_per_page);
        }
        else
        {
            $records = Permission::where('slug', '!=', 'superuser')
                ->orderBy('slug', 'asc')
                ->orderBy('label', 'asc')
                ->paginate($this->_per_page);
        }*/

        // Custom actions  in the form of ('key' => 'value')
        // to be handled by the 'action' method defined below.
        $actions = array();

        return View::make($this->_view_index,
            compact('records', 'sortby', 'order', 'page', 'actions')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make($this->_view_create);
    }

    // ------------------------------------------------------------------------

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Validate
        $validator = Validator::make(Input::all(), $this->_rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_create)
                ->withErrors($validator)
                ->withInput();
        }

        $permission = new Permission;
        $permission->label = Input::get('label');
        $permission->slug = Str::slug(Input::get('slug'), '.');
        $permission->save();

        // Redirect
        return $this->_afterSave($permission->label, $permission->id);
    }

    // ------------------------------------------------------------------------

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        echo 'Not implemented';
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // Get the data
        $permission = Permission::find($id);

        // Show view
        return View::make($this->_view_edit,
            compact('permission')
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Validate
        $validator = Validator::make(Input::all(), $this->_rules);

        if ($validator->fails())
        {
            return Redirect::route($this->_route_edit, $id)
                ->withErrors($validator)
                ->withInput();
        }

        $permission = Permission::find($id);
        $permission->label = Input::get('label');
        $permission->slug  = Str::slug(Input::get('slug'), '.');
        $permission->save();

        // Redirect
        return $this->_afterSave($permission->label, $permission->id);
    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Do not allow 'superuser' to be deleted
        if ($id == 1) {
            // Return JSON
            return Response::json(array('result'=>false));
        }

        // Delete record
        $record = Permission::find($id);
        $record->delete();

        // Return JSON
        return Response::json(array('result'=>true));
    }

    // ------------------------------------------------------------------------

    /**
     * Export the resource to a CSV file.
     *
     * @return Response
     */
    public function export() {
        $filename = 'permissions.'.date('Y-m-d');

        $results = Permission::all();

        $column_headers = array(
            'ID',
            'Label',
            'Slug',
            'Created',
            'Updated'
        );

        $fp = fopen('php://output', 'w');
        fputcsv($fp, $column_headers);
        foreach ($results as $_row) {
            fputcsv($fp, $_row->toArray());
        }
        $output = stream_get_contents($fp);
        fclose($fp);

        // Output CSV
        $headers = array(
            'Content-Type'        => 'text/csv; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="'.$filename.'.csv"',
        );

        return Response::make(rtrim($output, "\n"), 200, $headers);
    }

    // ------------------------------------------------------------------------

}
