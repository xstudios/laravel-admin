@extends('Xstudios\Laravel\Admin::users/base')

@section('breadcrumbs')
    @parent
    <li class="active">Create</li>
@stop

@section('page-header')
    Create User
@stop

@section('content')
    @parent

    <!-- if there are validation errors, they will show here -->
    @if ($errors->all())
    <div class="alert alert-danger">
    {{ HTML::ul($errors->all()) }}
    </div>
    @endif

    {{ Form::open(array('route' => 'admin.users.index', 'class' => 'form-horizontal', 'files' => true)) }}

        <div class="well">
            <fieldset>
                <legend>Personal Info</legend>

                {{ Form::bootstrapTextControl('First Name', 'first_name') }}
                {{ Form::bootstrapTextControl('Last Name', 'last_name') }}
                {{ Form::bootstrapTextControl('Email', 'email') }}

            </fieldset>
        </div>

        <div class="well">
            <fieldset>
                <legend>Permissions</legend>

                    {{ Form::bootstrapCheckboxControl('Active', 'activated', 1, true, 'Designates whether this user should be treated as active. Unselect this instead of deleting accounts.') }}
                    {{ Form::bootstrapCheckboxControl('Staff status', 'is_staff', 1, Input::old('is_staff'), 'Designates whether the user can log into this admin site.') }}

                    @if (Sentry::getUser()->isSuperUser())
                    {{ Form::bootstrapCheckboxControl('Superuser status', 'is_superuser', 1, Input::old('is_superuser'), 'Designates that this user has all permissions without explicitly assigning them.') }}
                    @endif

                    <div class="form-group">
                        {{ Form::label('groups', 'Groups', array('class'=>'col-sm-2')) }}
                        <div class="col-sm-10">
                            {{ Form::select('group-select[]', $groups, null, array('class' => 'form-control', 'multiple' => 'multiple', 'id' => 'group-select')) }}
                            <a href='#' id='group-select-all'>Select All</a> | <a href='#' id='group-deselect-all'>Deselect All</a>
                        </div>
                    </div>

                    @if (Sentry::getUser()->hasAccess('user.permission.assign'))
                    <hr />

                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-info">
                            <strong>NOTE:</strong> By choosing a Group above, the User inherits all permissions of the Group (<em>will not be shown below until saved</em>).
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('permissions', 'Permissions', array('class'=>'col-sm-2')) }}
                        <div class="col-sm-10">
                            {{ Form::select('permission-select[]', $permissions, null, array('class' => 'form-control', 'multiple' => 'multiple', 'id' => 'permission-select')) }}
                            <a href='#' id='select-all'>Select All</a> | <a href='#' id='deselect-all'>Deselect All</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-info">
                            <strong>NOTE:</strong> User permissions override any Group permissions.
                            </div>
                        </div>
                    </div>
                    @endif

            </fieldset>
        </div>

        <div class="well">
            <fieldset>
                <legend>Password</legend>

                {{ Form::bootstrapPasswordControl('Password', 'password') }}
                {{ Form::bootstrapPasswordControl('Confirm Password','password_confirmation') }}

            </fieldset>
        </div>

        <div class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container-fluid admin-form-controls">
                {{ Form::bootstrapSaveButtons('admin.users.index') }}
                <div class="clearfix"></div>
            </div>
        </div>

    {{ Form::close() }}

@stop

@section('css')
    {{ HTML::style('packages/xstudios/laravel-admin/css/multi-select.css') }}
@stop

@section('body_js')
    @include('Xstudios\Laravel\Admin::partials/permissions_select')
@stop

@section('footer')

@stop
