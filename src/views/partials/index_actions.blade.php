<div class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container-fluid admin-form-controls">
        <div class="col-md-3">
            <select class="form-control" id="select-action" name="action">
                <option value="">Select a Group Action</option>
                <option value="delete">Delete</option>
                @foreach($actions as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
