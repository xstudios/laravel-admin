<?php

class SentrySeeder extends Seeder {

	public function run()
	{
		//DB::table('users')->truncate();
		//DB::table('groups')->truncate();
		//DB::table('users_groups')->truncate();

		// Create my account
		Sentry::getUserProvider()->create(array(
			'email'       => 'root@domain.com',
			'password'    => 'password',
			'first_name'  => 'Root',
			'last_name'   => 'Admin',
			'activated'   => 1,
			'is_staff'    => 1,
			'permissions' => array('superuser' => 1)
		));

		// Create client account
		Sentry::getUserProvider()->create(array(
			'email'       => 'admin@domain.com',
			'password'    => 'password',
			'first_name'  => 'Admin',
			'last_name'   => 'Admin',
			'activated'   => 1,
			'is_staff'    => 1,
			'permissions' => array()
		));

		$default_perms = Permission::generateDefaultPermissions();
		$group_perms = array();
		foreach($default_perms as $perm)
		{
			$group_perms[$perm['slug']] = 1;
		}

		Sentry::getGroupProvider()->create(array(
			'name' => 'Admin',
			'permissions' => $group_perms
		));

		// Assign user permissions
		$adminUser  = Sentry::getUserProvider()->findByLogin('admin@domain.com');
		$adminGroup = Sentry::getGroupProvider()->findByName('Admin');
		$adminUser->addGroup($adminGroup);
	}

}
