<div id="header">
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::route('admin') }}">{{ Config::get('Xstudios\Laravel\Admin::config.admin_title') }}</a>
            </div>

            <div class="navbar-collapse collapse">
                @if (Sentry::check())
                <ul class="nav navbar-nav">

                    @section('main-nav')
                        @include('admin/nav')
                    @show

                    @if (Sentry::getUser()->hasAnyAccess(array('user.access', 'group.access')))
                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Administration <b class="caret"></b></a>

                        <ul class="dropdown-menu">

                            @if (Sentry::getUser()->hasAccess('user.access'))
                            <li class="{{ Active::route('admin.users') }}">
                                <a href="{{ URL::route('admin.users.index') }}"><span class="glyphicon glyphicon-user"></span> Users</a>
                            </li>
                            @endif

                            @if (Sentry::getUser()->hasAccess('group.access'))
                            <li class="{{ Active::route('admin.groups') }}">
                                <a href="{{ URL::route('admin.groups.index') }}"><span class="glyphicon glyphicon-user"></span> Groups</a>
                            </li>
                            @endif

                            @if (Sentry::getUser()->isSuperUser())
                            <li class="{{ Active::route('admin.permissions') }}">
                                <a href="{{ URL::route('admin.permissions.index') }}"><span class="glyphicon glyphicon-ok"></span> Permissions</a>
                            </li>
                            @endif

                        </ul>
                    </li>
                    @endif
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Sentry::check())
                    <li><a href="{{ URL::route('admin.logout') }}"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                    @endif
                </ul>
                @endif
            </div> <!--/.nav-collapse -->

        </div> <!--/.container-fluid -->

        @if (Sentry::check())
        <ol class="breadcrumb">
            @section('breadcrumbs')
            <li><a href="{{ URL::route('admin') }}">Home</a></li>
            @show
        </ol>
        @endif

    </div> <!--/.navbar -->
</div>
