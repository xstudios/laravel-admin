@extends('Xstudios\Laravel\Admin::groups/base')

@section('breadcrumbs')
    @parent
    <li class="active">Edit</li>
@stop

@section('page-header')
    Edit Group
@stop

@section('content')
    @parent

    <!-- if there are validation errors, they will show here -->
    @if ($errors->all())
    <div class="alert alert-danger">
    {{ HTML::ul($errors->all()) }}
    </div>
    @endif

    {{ Form::model($group, array('route' => array('admin.groups.update', $group->id), 'method' => 'PUT', 'class'=>'form-horizontal')) }}

        <div class="well">
            {{ Form::bootstrapTextControl('Name', 'name') }}

            <div class="form-group">
                {{ Form::label('permissions', 'Permissions', array('class'=>'col-sm-2')) }}
                <div class="col-sm-10">
                    {{ Form::select('permission-select[]', $permissions, $selected_perms, array('class' => 'form-control', 'multiple' => 'multiple', 'id' => 'permission-select')) }}
                    <a href='#' id='select-all'>Select All</a> | <a href='#' id='deselect-all'>Deselect All</a>
                </div>
            </div>
        </div>

        <div class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container-fluid admin-form-controls">
                {{ Form::bootstrapSaveButtons('admin.groups.index') }}
                <div class="clearfix"></div>
            </div>
        </div>

    {{ Form::close() }}

@stop

@section('css')
    {{ HTML::style('packages/xstudios/laravel-admin/css/multi-select.css') }}
@stop

@section('body_js')
    @include('Xstudios\Laravel\Admin::partials/permissions_select')
@stop

@section('footer')

@stop
