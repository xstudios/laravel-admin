@if (Session::has('message'))
<div class="alert alert-{{ Session::get('message.class', 'success') }} alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('message.message') }}
</div>
@endif
