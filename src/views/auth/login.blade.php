@extends('Xstudios\Laravel\Admin::layout.base')

@section('header')
@stop

@section('content')

    @if ($errors->has('login'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ $errors->first('login', ':message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="text-center login-title">
                <div class="account-wall">
                    <img src="{{ URL::asset('packages/xstudios/laravel-admin/img/login.png') }}" class="profile-img">
                    <h1>Admin Login</h1>
                    {{ Form::open(array('class'=>'form-signin')) }}
                        {{ Form::text('email', '', array('class'=>'form-control', 'placeholder' => 'Email')) }}
                        {{ Form::password('password', array('class'=>'form-control', 'placeholder' => 'Password')) }}
                        {{ Form::submit('Login', array('class' => 'btn btn-lg btn-primary btn-block')) }}
                        <span class="clearfix"></span>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <!--<div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            @if ($errors->has('login'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $errors->first('login', ':message') }}
            </div>
            @endif
        </div>
    </div>-->

@stop

@section('footer')
@stop
