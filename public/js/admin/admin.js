/* globals tinymce, console */
/**
 * App Admin
 *
 * @author      Tim Santor <tsantor@xstudiosinc.com>
 * @copyright   2014 X Studios
 * @link        http://www.xstudiosinc.com
 */
var Admin = function() {

    // ------------------------------------------------------------------------
    // INIT
    // ------------------------------------------------------------------------
    var __construct__ = function() {
        //xs_debug.trace('Admin::__construct__');
        // init tiny mce
        //initTinyMce();
    };

    // ------------------------------------------------------------------------
    // DELETE ROW
    // ------------------------------------------------------------------------
    this.deleteRow = function(url, dom_obj) {

        $('.alert-message').html('This will permanently delete <strong>"<span id="record-name"></span>"</strong>. Are you sure?');

        // trigger bootstrap modal
        $('#action-confirmation-modal').modal();

        // remove click event
        $('#modal-btn-confirm').unbind('click');

        // assign click event
        $('#modal-btn-confirm').click(function() {

            // hide modal
            $('#action-confirmation-modal').modal('hide');

            $.ajax({
                type: "DELETE",
                url: url,
                success: function(data) {
                    if (data.result === true) {
                        $(dom_obj).attr('disabled', 'disabled');
                        // remove table row
                        $(dom_obj).parent().parent().fadeOut('slow', function() {
                            $(this).remove();
                        });
                    }
                }
            });

        });

    };

    // ------------------------------------------------------------------------
    // TINY MCE
    // ------------------------------------------------------------------------
    this.initTinyMce = function() {
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    };

    // ------------------------------------------------------------------------
    // DATEPICKER
    // ------------------------------------------------------------------------
    this.initDatepicker = function() {
        $('.datepicker').datepicker();
    };

    // ------------------------------------------------------------------------
    // SUBMIT CUSTOM INDEX ACTION
    // ------------------------------------------------------------------------
    $('#select-action').on('change', function() {
        if ($(this).val() !== '') {
            $('.alert-message').html('<strong>This action is not undoable.</strong> Are you sure?');

            // trigger bootstrap modal
            $('#action-confirmation-modal').modal();

            // remove click event
            $('#modal-btn-confirm').unbind('click');

            // assign click event
            $('#modal-btn-confirm').click(function() {

                // hide modal
                $('#action-confirmation-modal').modal('hide');

                $('#form-records').submit();

            });
        }
    });

    // ------------------------------------------------------------------------
    // SELECT ALL RECORDS
    // ------------------------------------------------------------------------
    $('#select-all-records').on('change', function() {
        if (this.checked) {
            $('input[type=checkbox]').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });

    // ------------------------------------------------------------------------
    __construct__();
};

var admin = new Admin();
