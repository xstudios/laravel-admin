<?php

class PermissionSeeder extends Seeder {

	public function run()
	{
		//DB::table('permissions')->truncate();

		// Create all default permissions
		Permission::create(array(
			'label' => 'Super User (All Permissions)',
			'slug'  => 'superuser'
		));

		Permission::create(array(
			'label' => 'Can assign User Permissions',
			'slug'  => 'user.permission.assign'
		));

		$default_perms = Permission::generateDefaultPermissions();
		foreach($default_perms as $perm)
		{
			Permission::create($perm);
		}

	}

}
