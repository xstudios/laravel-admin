{{ HTML::script('packages/xstudios/laravel-admin/js/plugins/jquery.multi-select.js') }}
<script>
    $(function() {
        // ------------------------------------------------------------------------
        // Permissions mulit-select
        // ------------------------------------------------------------------------
        $('#permission-select').multiSelect({
            selectableHeader: '<label class="ms-label">Available Permissions</label>',
            selectionHeader: '<label class="ms-label">Selected Permissions</label>',
        });

        $('#select-all').click(function(){
            $('#permission-select').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function(){
            $('#permission-select').multiSelect('deselect_all');
            return false;
        });

        // ------------------------------------------------------------------------
        // Group mulit-select
        // ------------------------------------------------------------------------
        $('#group-select').multiSelect({
            selectableHeader: '<label class="ms-label">Available Groups</label>',
            selectionHeader: '<label class="ms-label">Selected Groups</label>',
        });

        $('#group-select-all').click(function(){
            $('#group-select').multiSelect('select_all');
            return false;
        });
        $('#group-deselect-all').click(function(){
            $('#group-select').multiSelect('deselect_all');
            return false;
        });

        // ------------------------------------------------------------------------
    });
</script>
