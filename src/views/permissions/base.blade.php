@extends('Xstudios\Laravel\Admin::layout/base')

@section('breadcrumbs')
    @parent
    <li><a href="{{ URL::route('admin.permissions.index') }}">Permissions</a></li>
@stop
