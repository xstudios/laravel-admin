<div class="modal fade" id="action-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="action-confirmation-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="action-confirmation-modalLabel">
                    Confirm Action
                </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-message">
                    Ask confirmation question here by replacing <code>.alert-message</code> with new content.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="modal-btn-confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>
