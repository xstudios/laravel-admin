<?php namespace Xstudios\Laravel\Admin;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

    const PACKAGE_ALIAS = 'Xstudios\Laravel\Admin';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $config;

    // ------------------------------------------------------------------------

    protected function getServiceProviderConfig() {
        return Config::get(static::PACKAGE_ALIAS . '::config');
    }

    // ------------------------------------------------------------------------

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('xstudios/laravel-admin', static::PACKAGE_ALIAS, __DIR__.'/../../..');

        // Include our custom routes and filters for this package
        include __DIR__.'/../../../routes.php';
        include __DIR__.'/../../../filters.php';
    }

    // ------------------------------------------------------------------------

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    // ------------------------------------------------------------------------

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    // ------------------------------------------------------------------------

}
