<?php

class AdminAuthController extends Controller {

    // ------------------------------------------------------------------------

    /**
     * Display the login page
     * @return View
     */
    public function getLogin()
    {
        return View::make('Xstudios\Laravel\Admin::auth.login');
    }

    // ------------------------------------------------------------------------

    /**
     * Login action
     * @return Redirect
     */
    public function postLogin()
    {
        $credentials = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        );

        try
        {
            $user = Sentry::authenticate($credentials, false);

            if ($user)
            {
                // Check if user is staff
                if ($user->is_staff)
                {
                    return Redirect::route(Config::get('Xstudios\Laravel\Admin::config.default_route'));
                }
                else
                {
                    Sentry::logout();
                    return Redirect::route('admin.login')->withErrors(array('login' => 'You are not a staff member.'));
                }
            }
        }
        catch(\Exception $e)
        {
            return Redirect::route('admin.login')->withErrors(array('login' => $e->getMessage()));
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Logout action
     * @return Redirect
     */
    public function getLogout()
    {
        Sentry::logout();

        return Redirect::route('admin.login');
    }

    // ------------------------------------------------------------------------

}
