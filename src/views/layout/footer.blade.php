<div id="footer">
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">

        <!--<div class="container-fluid">-->

            <p class="navbar-text">
                Copyright &copy; {{ date('Y') }}
            </p>

            <ul class="nav navbar-nav pull-right">
                @if (Sentry::check())
                <li><a href="{{ URL::route('admin.logout') }}"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                @endif
            </ul>

        <!--</div>-->

    </div>
</div>
