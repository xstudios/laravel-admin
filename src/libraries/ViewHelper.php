<?php

/**
 * X Studios Laravel View Helpers
 *
 * @author      Tim Santor <tsantor@xstudiosinc.com>
 * @version     1.0
 * @copyright   2014 X Studios
 * @link        http://www.xstudiosinc.com
 */

// ------------------------------------------------------------------------

class ViewHelper {
	/**
	 * Returns the proper link to enable column sorting in our
	 * crud's index view.
	 *
	 * @param  [type]  $name   [description]
	 * @param  [type]  $column [description]
	 * @param  [type]  $sortby [description]
	 * @param  [type]  $order  [description]
	 * @param  integer $page   [description]
	 * @param  [type]  $route  [description]
	 * @return [type]          [description]
	 */
	public static function sortable($name, $column, $sortby, $order, $page, $route) {

		if ($sortby == $column && $order == 'asc')
		{
			return link_to_route($route, $name,
				array(
					'sortby' => $column,
					'order'  => 'desc',
					'page'   => $page
				)
			);
		}
		elseif ($sortby == $column && $order == 'desc')
		{
			return link_to_route($route, $name,
				array(
					'sortby' => $column,
					'order'  => 'asc',
					'page'   => $page
				)
			);
		}
		else
		{
			return link_to_route($route, $name,
				array(
					'sortby' => $column,
					'order'  => 'desc',
					'page'   => $page
				)
			);
		}

	}

}
